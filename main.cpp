#include <iostream>
#include <string>

#include <google/protobuf/arena.h>
#include <google/protobuf/message.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/text_format.h>

#include "maq.h"
#include "reqrep.pb.h"
#include "auth.pb.h"

using namespace im::maq;

slice_ref_uint8_t msg_encode(const google::protobuf::Message& msg) {
    auto len = msg.ByteSizeLong();
    auto ptr = new uint8_t[len];

    msg.SerializeToArray(ptr, len);

    return {
            ptr,
            len
    };
}

int main() {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    auto* c = new_client("https://matrix.encom.eu.org", "/tmp/maq/db", "/tmp/maq/cache");

    {
        reqrep::Req req;
        auto* authorize = new reqrep::Authorize::Req;
        req.set_allocated_authorize(authorize);

        int req_c = client_send_req(c, msg_encode(req));
        std::cout << "c: " << req_c << std::endl;
    }

    auto status = auth::AuthStatus::UNKNOWN;

    while(status != auth::AuthStatus::READY) {
        reqrep::Rep rep;

        {
            reqrep::Req req;
            auto* status = new reqrep::AuthorizationStatus::Req;
            req.set_allocated_authorizationstatus(status);

            int req_c = client_send_req(c, msg_encode(req));
            std::cout << "c: " << req_c << std::endl;

            auto rep_vec = client_recv_rep(c, 10);
            rep.ParseFromArray(rep_vec.ptr, rep_vec.len);

            std::cout << "rep: " << rep.DebugString() << std::endl;
        }

        status = rep.authorizationstatus().status();

        switch (status) {
            case auth::AuthStatus::WAIT_USERNAME: {
                std::string user_identifier{"test"};

                reqrep::Req req;
                auto* send_user_identifier = new reqrep::SendUserIdentifier::Req;
                send_user_identifier->set_user_identifier(user_identifier);
                req.set_allocated_senduseridentifier(send_user_identifier);

                int req_c = client_send_req(c, msg_encode(req));

                std::cout << "c: " << req_c << std::endl;
            }
                break;
            case auth::AuthStatus::WAIT_PASSWORD: {
                std::string password{"testpassword"};

                reqrep::Req req;
                auto* send_password = new reqrep::SendPassword::Req;
                send_password->set_password(password);
                req.set_allocated_sendpassword(send_password);

                int req_c = client_send_req(c, msg_encode(req));

                std::cout << "c: " << req_c << std::endl;
            }
            break;
        }
    }

    std::cout << "Ready" << std::endl;

    return 0;
}
